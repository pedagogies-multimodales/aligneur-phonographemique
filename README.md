Ceci est un petit aligneur phonographémque de texte standalone.

Il utilise l'aligneur phonographémique [Phon2Graph](https://gricad-gitlab.univ-grenoble-alpes.fr/pedagogies-multimodales/phon2graph), l'analyseur morphosyntaxique [SpaCy](https://spacy.io/), et le dictionnaire phonétisé <code>dico_frwiktionary-20200301_v2.json</code> de [WikiPhon](https://gricad-gitlab.univ-grenoble-alpes.fr/pedagogies-multimodales/wikiphon).

Assurez-vous que SpaCy et le modèle de langue est bien installé :

<code>pip install -U spacy</code>

<code>python -m spacy download fr_core_news_md</code>


Pour lancer le programme, exécutez simplement le fichier <code>app.py</code>.