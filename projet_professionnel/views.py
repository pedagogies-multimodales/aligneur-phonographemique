import re, spacy, textphonographer
from liaisons import *
nlpFr = spacy.load('fr_core_news_md')

def colorize(text):

######### traitement du tiret ############
    nlpText1 = nlpFr(text)
    listeMots = []

    for i, mot in enumerate(nlpText1):
        # Traiter les mots qui contiennent le '-'.
        # Ex : "-il", "Peut-être"
        if "-" in mot.text and len(mot.text) > 1 :
            temp = ""
            for l in mot.text :
                if l != "-":
                    temp += l
                else:
                    if temp == '':
                        listeMots.append("-")
                    else:
                        listeMots.append(temp)
                        listeMots.append("-")
                        temp = ""
            if temp != "":
                listeMots.append(temp)
        # Traiter uniquement le tiret
        else:
            listeMots.append(mot.text)

    tiret = {} # EX : Vas-y ! Pouvait-il partir ? ==> {'Vas': 1, 'Pouvait': 5}
    for k,item in enumerate(listeMots):
        if item == "-" and k > 0 :
            tiret[listeMots[k-1]] = k
    print(tiret)

    # Remplacer tous les tirets dans le texte par le caractere espace
    if "-" in listeMots:
        text = text.replace('-', ' ')
#########################################

    nlpText = nlpFr(text)

    outText = []

    # appeler la fonction pour savoir s'il y a une locution
    index = liaison_locution(text, nlpText)
    print("Index", index)

    for j, token in enumerate(nlpText):

        liaison = None
        print("Mot en entrée :",token.text)

        if j + 1 < len(nlpText):

            # ici on met la liaison directement s'il s'agit d'une locution avec exception
            if index != None and (index[0]+index[1]) == j:
                    bigram = (nlpText[j].text, nlpText[j+1].text)
                    liaison = index[2]
                    print("bigram de locution :",bigram)

            # appel de la fonction obtenirM1M2
            else:
                bigram = obtenirM1M2(token.text, nlpText[j+1].text)
                print("bigram normal :",bigram)

                # vérifie les règles de liaisons et denasalisation
                if bigram != None:
                    print("bigram normal :",token.pos_,nlpText[j+1].pos_)
                    liaison = verifliaison(token, nlpText[j+1])
                    print("type de liaison : ",liaison)


        result = textphonographer.mimi(token.text,liaison) # LIAISON : ajoute le booléen liaison en argument
        print(result)

        phonographieList = []
        for r in result:      
            phonographie = []
            for i in r[0]:
                ph = {}
                ph['phon'] = i[0]
                ph['graph'] = i[1]
                phonographie.append(ph)
            phonographieList.append((phonographie,r[1],r[2],r[3]))
        outText.append(phonographieList)

        ## Affichage de la liaison hors du mot
        juge = False
        if len(tiret) > 0 and token.text in tiret.keys() :
            if liaison != None :
                result1 = textphonographer.mimi("-͜",token.text+liaison)
                juge = True
            elif liaison == None :
                result1 = textphonographer.mimi("-", liaison)
                juge = True
        else:
            if liaison != None:
                result1 = textphonographer.mimi("‿", token.text+liaison)
                juge = True

        if juge == True :
            phonographieList = []
            for r in result1:
                phonographie = []
                for i in r[0]:
                    ph = {}
                    ph['phon'] = i[0]
                    ph['graph'] = i[1]
                    phonographie.append(ph)
                phonographieList.append((phonographie, r[1], r[2], r[3]))
            outText.append(phonographieList)

    return outText
