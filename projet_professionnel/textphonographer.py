import json, re
from phon2graph import decoupage
from liaisons import *

# FICHIERS
phonColFile = "data/api2class.json" # api → class phonème
phonGraphFile = "data/fidel_wikicolor.scsv" # liste des graphies pour chaque phonème
dicFile = "data/dico_frwiktionary-20200301_v2.json" # dictionnaire phonétisé


# LECTURE DU CODE API-CLASS
with open(phonColFile,"r", encoding='UTF-8') as phonFile:
    phon2class = json.load(phonFile)


# LECTURE DE LA LISTE PHONEME-GRAPHIES (FIDEL)
phonFile = open(phonGraphFile,mode="r", encoding='UTF-8')
phon2graphFr = {}
phonCpt = 0
graphCpt = 0

for line in phonFile:
    phonCpt+=1
    line = line.strip()
    l= line.split(':')

    phon2graphFr[l[0]] = []

    listegraphies = l[1].split(',')
    for graph in listegraphies:
        phon2graphFr[l[0]].append(graph.replace("'","’"))
        graphCpt+=1

phonFile.close()


# LECTURE DU DICTIONNAIRE
word2transFr = {} # un mot → liste de trans possibles
with open(dicFile, 'r', encoding='UTF-8') as f:
    word2transFr = json.load(f)
lenDic = 0
for k in word2transFr.keys():
    lenDic+=1
print("Nombre d'entrées dans le dictionnaire :",lenDic)

def getLenDic():
    lenDic = 0
    for _ in word2transFr.keys():
        lenDic+=1
    return lenDic


def mimi(mot,liaison):  # LIAISON : avec le caractere liaison en argument ('O', 'F', 'N' ou None)
    word2trans = word2transFr
    phon2graph = phon2graphFr
    
    # Enregistrement de la casse
    caseMemory = []
    for i,lettre in enumerate(mot):
        if lettre.isupper():
            caseMemory.append(i)
            print('caseMemory +=',i,'(',lettre,')')
    mot = mot.lower()

    # Traitement de l'apostrophe
    mot = mot.replace("'",'’')
    
    
    # Si mot dans dictionnaire... decoupage(mot,transcription)
    if re.match(r'^\W+$|^\d+$',mot):
        print("'", mot, "' n'est pas un mot.")

        #verifier la liaison
        if '‿' in mot or "-͜" in mot :
            phon = phon_liaison(liaison[-2])
            if liaison[-1] == "O" :
                result = [([(phon, mot)], "", mot, "Liaison obligatoire")]
            elif liaison[-1] == "F" :
                result = [([(phon, mot)], "", mot, "Liaison facultative")]
            elif liaison[-1] == "N" :
                phon = "v"
                result = [([(phon, mot)], "", mot, "Liaison obligatoire")]
        else :
            result = [([('phon_neutre',mot)],"",mot,"")]

    elif mot in word2trans.keys():
        print("'", mot, "' trouvé dans le dico !",word2trans[mot])
        transList = word2trans[mot]
        result = []

        ############ partie d'appel de la fonction denasalisation
        if liaison != None :
            liste = ["aucun", "bien", "en", "on", "rien", "un", "non", "mon", "ton", "son","commun"]
            if mot[-1] == 'n' and mot not in liste :
                for i , trans in enumerate(transList):
                    trans = denasal(trans)
                    transList[i] = trans

        ############################################################

        for trans in transList:
            res = decoupage(mot,trans,phon2graph,phon2class)
            msg = ""
            ll = "Fr"
            tt = trans
            
            result.append((res,ll,tt,msg))

    else:
        print("'", mot, "' non trouvé !")
        result = [([('phon_inconnu',mot)],"","","Mot non trouvé dans le dictionnaire")]

    # Rétablissement de la casse
    for r in result:
        if len(r[0])>0:
            for m in caseMemory:
                cptlettre = 0
                while cptlettre < len(mot):
                    for k,tupl in enumerate(r[0]):
                        #print("\t",tupl)
                        cased = ''
                        for l in tupl[1]:
                            cased += l.upper() if cptlettre == m else l
                            cptlettre += 1
                        r[0][k] = (tupl[0],cased)
    
    return result

