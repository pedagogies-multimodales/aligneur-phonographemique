#########################################
#     Documentation pour le projet      #
#__Encodage des liaisons sur Wikicolor__#
# Sanda HACHANA                         #
# Cynthia RAKOTOARISOA                  #   
# Ning ZHANG                            #  
#                                  2021 #
#########################################

------>liaisons.py


def liaison_locution(): #ligne 230
______Fonction pour vérifier sˈil y une locution dans le texte brut
######
EN ENTREE :
    texte brut(str),
    texte spacy(spacy.tokens.doc.Doc)
######
DESCRIPTION :
    Un tableau contient des locutions où les liaisons existent, mais ne peuvent pas être identifiées par les règles. La clé du tableau consiste à la locution, et la valeur est un tuple qui contient lˈindice de liaison dans la locution et le type de liaison, qui sont indiqué manuellement.
    En fonction de cette liste, examine chaque mot du texte pour savoir à quel indice se trouve cette locution dans le texte.
######
EN SORTIE :
    1 tuple ou None
    - tuple :(
            int : indice de locution
            int : indice de liaison dans la locution
            str : type de liaison
             )
    - None quand il nˈy a pas de locution
######
APPEL DE LA FONCTION :
    dans la fonction def colorize() (dans le script views.py)
######
EXEMPLE :
    >>nlpFr = spacy.load('fr_core_news_md')
    >>text = "Comment allez-vous, Marie ?"
    >>nlpText = nlpFr(text)
    >>a = liaison_locution(text, nlpText)
    #a = (0, 0, 'O')

____________________________________________________

def obtenirM1M2(): #ligne 9
______Fonction qui va déterminer si une liaison est possible entre 2 mots
######
EN ENTREE :
    2 mots (str)
######
DESCRIPTION :
    examine une suite de 2 mots pour vérifier une possible liaison en fonction des conditions suivantes :
    - le 1er mot se termine par une de ces lettres ['s', 'x', 'z', 't', 'd', 'n', 'p', 'f', 'r', 'g']
    - le 2nd mot commence par une voyelle ou par un 'h' muet
######
EN SORTIE :
    tuple avec ces 2 mots, si la liaison est possible
    None, si elle nˈest pas possible
######
APPEL DE LA FONCTION :
    dans la fonction def colorize() (dans le script views.py)
######
EXEMPLE :
    >>a = obtenirM1M2('ces', 'histoires')
    #a = ('ces', 'histoires')
    >>b = obtenirM1M2('cette', 'chanson')
    #b = None

_____________________________________________________________

def exception(): #ligne 204
______Fonction pour trouver les cas dˈexception
######
EN ENTREE :
    2 tokens(spacy.tokens.token.Token)
######
DESCRIPTION :
    Les 5 cas sont traités :
    - si le deuxième token est une lettre voyelle
    - si le deuxième token fait partie de la liste ["oui", "ouf"]
    - si le deuxième token est un nom propre
    - si le mot "quand" est suivi par "est"
    - si le mot "comment" est suivi par "allez"
    Retourne False pour les cas ci-dessus, sinon retourne True
######
EN SORTIE :
    bool
######
APPEL DE LA FONCTION :
    dans la fonction def verifliaison()
######
EXEMPLE :
    >>a = exception(chez, Orange)
    #a = False
    
_____________________________________________________________

def gfr(): #ligne 369
______Fonction pour les mots qui se terminent en 'g' / 'f' / 'r'
######
EN ENTREE :
    1 mot(str)
#####
DESCRIPTION :
    verifie si le mot se termine par une de ses 3 lettres
    si se termine par 'r': renvoie True si le mot se termine par "er"
    si se termine par 'f': renvoie True pour le mot "neuf"
    si se termine par 'g': renvoie True pour le mot "long"
    si le mot se termine par 'g', 'f' ou 'r' mais nˈest pas vrai pour les 3 cas precedents alors renvoie False
#####
EN SORTIE :
    bool
######
APPEL DE LA FONCTION :
    dans la fonction def verifliaison()
#####
EXEMPLE :
    >> a = gfr("premier")
    # a = True
    >> b = gfr("actif")
    # b = False    

_____________________________________________________________

def liaison_nombre(): #ligne 133
______Fonction qui va examiner la liaison entre un chiffre et un autre mot et détermine le type de liaison ("O" ou "F" ou "N")
######
EN ENTREE :
    2 tokens(spacy.tokens.token.Token)
######
DESCRIPTION :
    Si le nombre fait partie de la liste ['un', 'une', 'deux', 'trois', 'onze', 'onzième','onzièmes', 'neuf', 'vingt', 'vingts', 'cent', 'cents'], examine la liaison selon de différents cas.
######
EN SORTIE :
    1 str ou None
    - 'O' pour liaison obligatoire
    - 'F' pour liaison facultative
    - 'N' pour liaison de "neuf"
    - None quand il nˈy a pas de liaison
######
APPEL DE LA FONCTION :
    dans la fonction def verifliaison()
######
EXEMPLE :
    >>a = liaison_nombre(deux,hommes)
    #a = 'O'
    >>b = liaison_nombre(pendant,un)
    #a = 'F'
    >>c = liaison_nombre(neuf,ans)
    #a = 'N'
    >>d = liaison_nombre(trois,avril)
    #a = None
    
_____________________________________________________________

def obl_facul(): #ligne 38
______Fonction qui va indiquer si une liaison est obligatoire (O) ou facultative (F)
######
EN ENTREE :
    2 tokens(spacy.tokens.token.Token)
######
DESCRIPTION :
    applique les règles générales de liaison et détermine en fonction :
    - de leur catégorie syntaxique
    - du mot
    si la liaison est obligatoire, facultative ou interdite
######
EN SORTIE :
    1 str ou None :
    - 'O' pour liaison obligatoire
    - 'F' pour liaison facultative
    - None quand il nˈy a pas de liaison
######
APPEL DE LA FONCTION :
    dans la fonction def verifliaison()
######
EXEMPLE :
    >>a = verifliaison(chez, elle)
    #a = 'O'
    >>b = verifliaison(est, un)
    #b = 'F'
    >>c = verifliaison(dessin, animé)
    #c = None

_____________________________________________________________

def verifliaison(): #ligne 336
______Fonction pour savoir si la liaison doit etre faite, et si oui comment (O ou F)
######
EN ENTREE :
    2 tokens(spacy.tokens.token.Token)
######
DESCRIPTION :
    appelle toutes les autres fonctions du script pour verifier sˈil y a liaison ou non
    verifie dans un premier temps tous les cas des exceptions
######
EN SORTIE :
    1 str ou None :
    - 'O' pour liaison obligatoire
    - 'F' pour liaison facultative
    - None quand il n y a pas de liaison
######
APPEL DE LA FONCTION :
    dans la fonction def colorize() (dans le script views.py)
#####
EXEMPLE :
    >> a = verifliaison("des", "amis")
    # a = 'O'
    >> b = verifliaison("des", "yaourts")
    # b = None

_____________________________________________________________

def phon_liaison(): #ligne 357
______Fonction qui va determiner le phoneme de liaison
######
EN ENTREE :
   1 caractere : la derniere lettre du mot
######
DESCRIPTION :
    chaque lettre est assignée à une prononciation (un phoneme)
    la prononciation de la liaison recupere le phoneme de la derniere lettre du mot
######
EN SORTIE :
   1 str : phoneme de liaison
######
APPEL DE LA FONCTION :
    dans la focntion def mimi() (dans le script textphonographer.py)
#####
EXEMPLE :
    >> a = phon_liaison('s')
    # a = 'phon_z'
    >> b = phon_liaison('m')
    # b = 'rien'


_____________________________________________________________

def denasal(): #ligne 119
______Fonction pour effectuer une dénasalisation
######
EN ENTREE :
    1 str : transcription phonétique
######
DESCRIPTION :
    prend la dernière voyelle de la transcription, si cˈest une voyelle nasale on la dénasalise
    en supprimant le dernier caractère ' ˜' et en ajoutant la consonne 'n'
######
EN SORTIE :
    1 str : transcription phonétique dénasalisée
######
APPEL DE LA FONCTION :
    dans la fonction def mimi() (dans le script textphonographer.py)
######
EXEMPLE :
    >>a = denasal('bɔ̃')
    #a = 'bɔn'

