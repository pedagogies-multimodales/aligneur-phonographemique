import json
from views import colorize

print("Bienvenue dans le simulateur Wikicolor-liaisons.")
inp = ''

while inp != ':q':
    print("Tapez du texte ou :q pour quitter l'application.")
    inp = input()

    if inp == ':q':
        print("Arrêt.")
        break
    else:
        result = colorize(inp)

        print('\n\n RESULTAT : \n')
        print(json.dumps(result, indent = 3, ensure_ascii=False))
        print('\n')