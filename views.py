import re, spacy, textphonographer
nlpFr = spacy.load('fr')

def colorize(text):
    nlpText = nlpFr(text)
    outText = []

    for token in nlpText:
        print("Mot en entrée :",token.text)
        result = textphonographer.mimi(token.text)
        print(result)
    
        phonographieList = []
        for r in result:      
            phonographie = []
            for i in r[0]:
                ph = {}
                ph['phon'] = i[0]
                ph['graph'] = i[1]
                phonographie.append(ph)
            phonographieList.append((phonographie,r[1],r[2],r[3]))
        outText.append(phonographieList)
    
    return outText